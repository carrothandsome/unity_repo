﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCheck : MonoBehaviour {

    private void OnMouseDown()
    {
        GameManager.instance.dragging = true;
    }

    private void OnMouseUp()
    {
        GameManager.instance.dragging = false;
    }
}
