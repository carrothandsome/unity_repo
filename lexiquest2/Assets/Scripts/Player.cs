﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private Vector3 position;
	// Use this for initialization
	void Start ()
    {
        position = transform.position;
        GetComponent<MeshRenderer>().material.SetColor("_Color", Color.green);
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetInput();
		
	}

    private void Move(Vector3 direction)
    {
        Ray ray = new Ray(transform.position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1f) && hit.collider.tag == "Wall")
        {
            return;
        }
        transform.Translate(direction);
    }
    private void GetInput()
    {
        if (Input.GetButtonDown("Up"))
        {
            Move(Vector3.up);
        }
        if (Input.GetButtonDown("Down"))
        {
            Move(Vector3.down);
        }
        if (Input.GetButtonDown("Left"))
        {
            Move(Vector3.left);
        }
        if (Input.GetButtonDown("Right"))
        {
            Move(Vector3.right);
        }
    }
}
