﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Random = UnityEngine.Random;

public class Letter : MonoBehaviour {

    public char letter;
    public TextMesh textMesh;
    public TMPro.TextMeshPro textMeshPro;
    public Vector3 position;

    private bool highlighted = false;
    private bool excited = false;
    private Renderer rend;

	// Use this for initialization
	private void Start ()
    {
        letter = GameManager.instance.GetRandomLetter();
        position = transform.position;
        textMeshPro.text = letter.ToString();
        rend = GetComponent<Renderer>();
	}

    public void Highlight(bool hl)
    {
        highlighted = hl;
        if (highlighted)
        {
            rend.material.SetColor("_Color", Color.red);
        }
        else
        {
            rend.material.SetColor("_Color", Color.white);
        }
    }

    public void Excite(bool e)
    {
        float shake = 0.05f;
        if (!excited && e)
        {
            excited = e;
            StartCoroutine(Shake(-shake, shake, -shake, shake, 0.01f));
        }
        else
        {
            excited = e;
        }
    }

    public IEnumerator Shake(float minX, float maxX, float minY, float maxY, float timeBetweenShakes)
    {
        while (excited)
        {      
            transform.position = position + new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0);
            yield return new WaitForSeconds(timeBetweenShakes);
        }

        transform.position = position;
    }
}
