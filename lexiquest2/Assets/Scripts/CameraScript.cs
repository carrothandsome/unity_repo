﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public Camera mainCamera;
    public Player player;
    public bool perspective = true;
    public Transform orthoTransform;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if ((Vector2)transform.position != (Vector2)player.transform.position)
        {
            transform.position = player.transform.position + Vector3.back * 15;
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (perspective)
            {
                perspective = false;
                transform.rotation = Quaternion.identity;
                mainCamera.orthographic = true;
            }
            else
            {
                perspective = true;
                transform.rotation = Quaternion.AngleAxis(-15, Vector3.right);
                mainCamera.orthographic = false;
            }
        }
	}
}
