﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public GameObject blockPrefab;
    public GameObject playerPrefab;
    public GameObject wallPrefab;

    public bool dragging = false;

    private Vector3 entrance;
    public Player player;
    private List<Letter> letters;
    private List<Letter> selectedLetters;

    public TextAsset words;
    public Dictionary<string, int> dictionary;

	// Use this for initialization
	void Awake ()
    {
		if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        
	}

    void Start()
    {
        letters = new List<Letter>();
        selectedLetters = new List<Letter>();
        //Load Dictionary
        dictionary = new Dictionary<string, int>();
        string[] linesFromWords = words.text.Split("\n"[0]);
        Debug.Log(linesFromWords.Length.ToString());
        
        foreach (string s in linesFromWords)
        {
            if (s.Length > 2)
            {
                dictionary.Add(s.ToUpper(), 0);
            }
        }

        int xSize = 20;
        int ySize = 20;

        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize;  y++)
            {
                GameObject newBlock = Instantiate(blockPrefab, new Vector3(x, y, 0), Quaternion.identity, instance.transform) as GameObject;
                letters.Add(newBlock.GetComponent<Letter>());
                
                if (x == 0 || y == 0 || x == xSize - 1 || y == ySize - 1)
                {
                    GameObject wall = Instantiate(wallPrefab, new Vector3(x, y, -1), Quaternion.identity, instance.transform) as GameObject;
                }
                else if (Random.Range(0, 1f) < 0.15f)
                {
                    GameObject wall = Instantiate(wallPrefab, new Vector3(x, y, -1), Quaternion.identity, instance.transform) as GameObject;
                }
            }
        }

        entrance = letters[ySize + 1].transform.position + Vector3.back;
        player.transform.position = entrance;

        //player = (Instantiate(playerPrefab, entrance, Quaternion.identity) as GameObject).GetComponent<Player>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            MouseDown();
        }
        if (Input.GetMouseButtonUp(0))
        {
            MouseUp();
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 30f))
        {
            if (hit.collider.tag == "Letter")
            {
                Letter hitLetter = hit.collider.gameObject.GetComponent<Letter>();

                if (selectedLetters.Count > 0 && CheckAdjacent(hitLetter, selectedLetters).Count < 2)
                {
                    //Debug.Log("clear");
                    ClearLetters();
                }

                //hitLetter.Highlight(true);
                if (dragging)
                {
                    GetLetter(hitLetter);
                }
            }
        }
    }

    public void FocusLetter(Letter letter)
    {
        letter.Highlight(true);
        foreach (Letter l in letters)
        {
            if (l != letter)
            {
                l.Highlight(false);
            }
        }
    }

    public void GetLetter(Letter letter)
    {
        if (!selectedLetters.Contains(letter)) //add new letter if it is not already in the list...
        {
            letter.Highlight(true);
            selectedLetters.Add(letter);

            if (CheckForWord(selectedLetters))
            {
                //Debug.Log("valid word");
                ExciteLetters(selectedLetters, true);
            }
            else
            {
                ExciteLetters(selectedLetters, false);
            }
        }
        else if (letter != selectedLetters[selectedLetters.Count - 1])//... or if it is at least not the last one in the list, revert the list back to end at that letter
        {
            List<Letter> revertedLetters = selectedLetters.Skip<Letter>(selectedLetters.IndexOf(letter) + 1).ToList<Letter>();
            List<Letter> newSelectedLetters = selectedLetters.Take<Letter>(selectedLetters.IndexOf(letter) + 1).ToList<Letter>();

            selectedLetters = newSelectedLetters;

            if (CheckForWord(selectedLetters))
            {
                //Debug.Log("valid word");
                ExciteLetters(selectedLetters, true);
            }
            else
            {
                ExciteLetters(selectedLetters, false);
            }

            foreach (Letter l in revertedLetters)
            {
                l.Highlight(false);
                l.Excite(false);
            }
        }
    }

    public bool CheckAdjacent(Letter letter1, Letter letter2) //checks if letter1 is cardinally adjacent to letter2
    {
        int x1 = (int)letter1.position.x;
        int x2 = (int)letter2.position.x;
        int y1 = (int)letter1.position.y;
        int y2 = (int)letter2.position.y;
        int xDiff = Mathf.Abs(x1 - x2);
        int yDiff = Mathf.Abs(y1 - y2);

        if ((xDiff == 0 && yDiff == 1)|| (yDiff == 0 && xDiff == 1) || (xDiff == 1 && yDiff == 1))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public List<Letter> CheckAdjacent(Letter letter, List<Letter> letters) //returns sublist of list of letters. sublist is all those adjacent to first letter argument
    {
        List<Letter> adjacentTo = new List<Letter>();
        foreach (Letter l in letters)
        {
            if (CheckAdjacent(letter, l))
            {
                adjacentTo.Add(l);
            }
        }
        adjacentTo.Add(letter); //letter is adjacent to itself

        return adjacentTo;
    }

    public bool CheckForWord(List<Letter> letters)
    {
        string word = "";
        foreach (Letter letter in letters)
        {
            word += letter.letter;
        }
        //Debug.Log(word);
        return (dictionary.ContainsKey(word));
    }

    public void ClearLetters()
    {
        foreach (Letter letter in selectedLetters)
        {
            letter.Highlight(false);
            letter.Excite(false);
        }
        selectedLetters.Clear();
    }

    private void ExciteLetters(List<Letter> letters, bool excite)
    {
        foreach (Letter l in letters)
        {
            l.Excite(excite);
        }
    }

    private void MouseDown()
    {
        dragging = true;
    }

    private void MouseUp()
    {
        dragging = false;
        ClearLetters();
    }

    public char GetRandomLetter()
    {
        int dist = 0;
        int[] distArray = { 16, 4, 6, 8, 24, 4, 5, 5, 13, 2, 2, 7, 6, 13, 15, 4, 2, 13, 10, 15, 7, 3, 4, 2, 4, 2 }; //distribution of 26 letters
        int[] dists = new int[26];
        for (int i = 0; i < 26; i++)
        {
            dist += distArray[i];
            dists[i] = dist;
        }

        int j = 0;
        int k = Random.Range(1, dist + 1);        

        while (dists[j] <= k)
        {
            j++;
            if (j == 26)
            {
                return 'Z';
            }
        }

        return (char)(j + 65);
    }
}
